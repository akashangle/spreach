// Include the SocketIO and WebRTC Scripts before using this Class
// <script src="https://webrtc.github.io/adapter/adapter-latest.js"></script>
// <script src="/socket.io/socket.io.js"></script>

class SpreachClient {
    constructor(SpreachServerUrl) {
        this.SpreachServerUrl = SpreachServerUrl;
        this.IsInitiator = false;
        this.PeerConnections = new Map();

        this.PeerConnectionsConfig = { iceServers: [{ urls: 'stun:stun.l.google.com:19302' }] };

        this.SpreachServerSocket = io.connect(this.SpreachServerUrl);
        this.SpreachServerSocket.on('connect', function () {
            console.log("[CLIENT_SOCKET] : CONNECT");

            this.SpreachServerSocket.on("CREATE_ACK", function (msg) {
                console.log("[CLIENT_SOCKET] : CREATE_ACK");
            });

            this.SpreachServerSocket.on("JOIN_ACK", function (msg) {
                console.log("[CLIENT_SOCKET] : JOIN_ACK");
            });

            this.SpreachServerSocket.on("OFFER_ACK", function (msg) {
                console.log("[CLIENT_SOCKET] : OFFER_ACK");
            });

            this.SpreachServerSocket.on("ANSWER_ACK", function (msg) {
                console.log("[CLIENT_SOCKET] : ANSWER_ACK");
            });

            this.SpreachServerSocket.on("JOIN_USER", function (msg) {
                console.log("[CLIENT_SOCKET] : JOIN_USER");
                // Create an offer and send it back to the user using the USER_ID in the msg to notify the correct user
                let parsedMsg = JSON.parse(msg);
                this.PeerConnections.set(parsedMsg.USER_ID, new RTCPeerConnection(this.PeerConnectionsConfig));
                if (this.PeerConnections.has(parsedMsg.USER_ID) == true) {
                    this.PeerConnections.get(parsedMsg.USER_ID).addStream(SelfStream.srcObject);
                    this.PeerConnections.get(parsedMsg.USER_ID).onaddstream = function (event) {
                        console.log("OnAddStream");
                        let RemoteStream = document.createElement("VIDEO");
                        RemoteStream.setAttribute("width", "200");
                        RemoteStream.setAttribute("height", "200");
                        RemoteStream.setAttribute("autoplay", "true");
                        document.getElementById('remoteStreamDiv').appendChild(RemoteStream);
                        RemoteStream.srcObject = event.stream;
                    };
                    this.PeerConnections.get(parsedMsg.USER_ID).onicecandidate = function (event) {
                        console.log("OnIceCandidateEvent");
                        if (event.candidate) {
                            console.log("EMICE_DEBUG : " + parsedMsg.USER_ID);
                            this.SpreachServerSocket.emit("ICE", JSON.stringify({ "WORKSPACE_NAME": parsedMsg.WORKSPACE_NAME, "USER_ID": parsedMsg.USER_ID, "ICE": event.candidate }));
                        }
                    }.bind(this);
                    this.PeerConnections.get(parsedMsg.USER_ID).createOffer().then(function (offer) {
                        this.PeerConnections.get(parsedMsg.USER_ID).setLocalDescription(new RTCSessionDescription(offer));
                        this.SpreachServerSocket.emit("OFFER", JSON.stringify({ "WORKSPACE_NAME": parsedMsg.WORKSPACE_NAME, "USER_ID": parsedMsg.USER_ID, "OFFER": offer }));
                    }.bind(this)).catch(function (error) {
                        console.log("[ERROR] : " + error);
                    });
                }
            }.bind(this));

            this.SpreachServerSocket.on("OFFER", function (msg) {
                console.log("[CLIENT_SOCKET] : OFFER");
                let parsedMsg = JSON.parse(msg);
                console.log(parsedMsg.USER_ID);
                this.PeerConnections.set(parsedMsg.USER_ID, new RTCPeerConnection(this.PeerConnectionsConfig));
                if (this.PeerConnections.has(parsedMsg.USER_ID) == true) {
                    this.PeerConnections.get(parsedMsg.USER_ID).addStream(SelfStream.srcObject);
                    this.PeerConnections.get(parsedMsg.USER_ID).onaddstream = function (event) {
                        console.log("OnAddStream");
                        let RemoteStream = document.createElement("VIDEO");
                        RemoteStream.setAttribute("width", "200");
                        RemoteStream.setAttribute("height", "200");
                        RemoteStream.setAttribute("autoplay", "true");
                        document.getElementById('remoteStreamDiv').appendChild(RemoteStream);
                        RemoteStream.srcObject = event.stream;
                    };
                    this.PeerConnections.get(parsedMsg.USER_ID).onicecandidate = function (event) {
                        console.log("OnIceCandidateEvent");
                        if (event.candidate) {
                            console.log("EMICE_DEBUG : " + parsedMsg.USER_ID);
                            this.SpreachServerSocket.emit("ICE", JSON.stringify({ "WORKSPACE_NAME": parsedMsg.WORKSPACE_NAME, "USER_ID": parsedMsg.USER_ID, "ICE": event.candidate }));
                        }
                    }.bind(this);
                    console.log(parsedMsg.OFFER);
                    this.PeerConnections.get(parsedMsg.USER_ID).setRemoteDescription(new RTCSessionDescription(parsedMsg.OFFER)).then(function() {
                        this.PeerConnections.get(parsedMsg.USER_ID).createAnswer().then(function (answer) {
                            this.PeerConnections.get(parsedMsg.USER_ID).setLocalDescription(new RTCSessionDescription(answer));
                            this.SpreachServerSocket.emit("ANSWER", JSON.stringify({ "WORKSPACE_NAME": parsedMsg.WORKSPACE_NAME, "USER_ID": parsedMsg.USER_ID, "ANSWER": answer }));
                        }.bind(this)).catch(function (error) {
                            console.log("[ERROR] : " + error);
                        });
                    }.bind(this));
                }
            }.bind(this));

            this.SpreachServerSocket.on("ANSWER", function (msg) {
                console.log("[CLIENT_SOCKET] : ANSWER");
                let parsedMsg = JSON.parse(msg);
                if (this.PeerConnections.has(parsedMsg.USER_ID) == true) {
                    console.log(parsedMsg.ANSWER);
                    this.PeerConnections.get(parsedMsg.USER_ID).setRemoteDescription(new RTCSessionDescription(parsedMsg.ANSWER));
                }
            }.bind(this));

            this.SpreachServerSocket.on("ICE", function (msg) {
                console.log("[CLIENT_SOCKET] : ICE");
                let parsedMsg = JSON.parse(msg);
                console.log("ONICE_DEBUG : " + parsedMsg.USER_ID);
                if (this.PeerConnections.has(parsedMsg.USER_ID) == true) {
                    console.log("HAS");
                    console.log(parsedMsg.ICE);
                    this.PeerConnections.get(parsedMsg.USER_ID).addIceCandidate(new RTCIceCandidate(parsedMsg.ICE));
                }
            }.bind(this));

            this.SpreachServerSocket.on("disconnect", function () {
                console.log("[CLIENT_SOCKET] : DISCONNECT");
            });
        }.bind(this));
    }

    CreateWorkspace(WorkspaceName) {
        console.log("[CLIENT] : CreateWorkspace");
        this.IsInitiator = true;
        this.SpreachServerSocket.emit('CREATE', JSON.stringify({"WORKSPACE_NAME": WorkspaceName}));
    }

    JoinWorkspace(WorkspaceName) {
        console.log("[CLIENT] : JoinWorkspace");
        if (this.IsInitiator == false) {
            this.SpreachServerSocket.emit('JOIN', JSON.stringify({"WORKSPACE_NAME": WorkspaceName}));
        }
        else {
            console.log("[ERROR] : Client is an Initiator");
        }
    }

    StartCall() {
        console.log("[CLIENT] : StartCall");
    }

    EndCall() {
        console.log("[CLIENT] : EndCall");
    }

    CloseWorkspace(WorkspaceName) {
        console.log("[CLIENT] : CloseWorkspace");
        this.SpreachServerSocket.emit('CLOSE', JSON.stringify({"WORKSPACE_NAME": WorkspaceName}));
        this.IsInitiator = false;
    }
};

//---------------------------------------------------------------------------------------------------
const SelfStream = document.getElementById("selfStream");
// const RemoteStream = document.getElementById("remoteStream");

const UserMediaConstraints = 
{
    audio: true,
    video: { width: 200, height: 200 }
};

navigator.getUserMedia(UserMediaConstraints, function (stream) 
{
    SelfStream.srcObject = stream;
}, function (error) 
{
    console.log("[ERROR] : Video Stream Not Supported");
});

const spreachServerUrl = "https://spreach.herokuapp.com";
// const spreachServerUrl = "localhost:5000"

let client = new SpreachClient(spreachServerUrl);

console.log("READY");

document.getElementById("CreateWorkspaceButton").addEventListener("click", function()
{
    console.log("[DOM] : CreateWorkspaceButtonClick");
    client.CreateWorkspace(document.getElementById('WorkspaceName').value);
});

document.getElementById("JoinWorkspaceButton").addEventListener("click", function()
{
    console.log("[DOM] : JoinWorkspaceButtonClick");
    client.JoinWorkspace(document.getElementById('WorkspaceName').value);
});

document.getElementById("CloseWorkspaceButton").addEventListener("click", function()
{
    console.log("[DOM] : CloseWorkspaceButtonClick");
    client.CloseWorkspace(document.getElementById('WorkspaceName').value);
});

document.getElementById("StartCallButton").addEventListener("click", function()
{
    console.log("[DOM] : StartCallButtonClick");
    client.StartCall();
});

document.getElementById("EndCallButton").addEventListener("click", function()
{
    console.log("[DOM] : EndCallButtonClick");
    client.EndCall();
});
